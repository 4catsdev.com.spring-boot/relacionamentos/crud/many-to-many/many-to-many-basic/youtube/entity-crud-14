package com.fourcatsdev.entitycrud.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.entitycrud.modelo.Habilidade;
import com.fourcatsdev.entitycrud.repositorio.HabilidadeRepositorio;

@Service
public class HabilidadeServico {
	
	@Autowired
	private HabilidadeRepositorio habilidadeRepositorio;
	
	public Habilidade criarHabilidade(Habilidade habilidade) {
		return habilidadeRepositorio.save(habilidade);
	}

}
